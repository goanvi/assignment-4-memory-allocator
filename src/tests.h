#ifndef TESTS
#define TESTS

#include <stdbool.h>
#include <sys/mman.h>

typedef bool (*my_test) (void);

extern my_test tests[];
extern size_t tests_count;
bool tester();

#endif
