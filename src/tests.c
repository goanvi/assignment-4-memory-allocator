#define _DEFAULT_SOURCE
#include "mem.h"
#include "mem_internals.h"
#include "tests.h"


#define HEAP_SIZE 8175
#define BLOCK_SIZE 1000


static void heap_free(void* heap) {
    munmap(heap, size_from_capacity((block_capacity){.bytes = HEAP_SIZE}).bytes);
}



bool malloc_block(){
	fprintf(stdout, "\n\n\tTest malloc_block\n\n---Init heap---\n");
	void* heap_addr = heap_init(HEAP_SIZE);
	debug_heap(stdout,heap_addr);
	if (heap_addr==NULL){
		fprintf(stderr, "\tHeap initialisation error\n");
		heap_free(heap_addr);
		return false;
	}
	fprintf(stdout, "\n---Memory allocation---\n");
	void* content  = _malloc(BLOCK_SIZE);
	struct block_header* block = block_get_header(content);
	debug_heap(stdout,heap_addr);
	if (content == NULL || block->capacity.bytes!=BLOCK_SIZE){
		fprintf(stderr, "\tMemory allocation error\n");
		heap_free(heap_addr);
		return false;
	}
	fprintf(stdout, "\n---Free memory---\n");
	_free(content);
	debug_heap(stdout, heap_addr);
	if (!block->is_free){
		fprintf(stderr, "\tMemory release error\n");
		heap_free(heap_addr);
		return false;
	}
	heap_free(heap_addr);
	return true;
}

bool free_one_block(){
	fprintf(stdout, "\n\n\tTest free_one_block\n\n---Init heap---\n");
	void* heap_addr = heap_init(HEAP_SIZE);
	debug_heap(stdout,heap_addr);
	fprintf(stdout, "\n---Memory allocation---\n");
	_malloc(BLOCK_SIZE);
	void* content2  = _malloc(BLOCK_SIZE);
	 _malloc(BLOCK_SIZE);
	struct block_header* block2 = block_get_header(content2);
	debug_heap(stdout,heap_addr);
	fprintf(stdout, "\n---Free second block---\n");
	_free(content2);
	debug_heap(stdout,heap_addr);	
	if (!block2->is_free){
		fprintf(stderr, "\tMemory release error\n");
		heap_free(heap_addr);
		return false;
	}
	heap_free(heap_addr);
	return true;
}

bool free_some_blocks(){
	fprintf(stdout, "\n\n\tTest free_some_blocks\n\n---Init heap---\n");
	void* heap_addr = heap_init(HEAP_SIZE);
	debug_heap(stdout,heap_addr);
	fprintf(stdout, "\n---Memory allocation---\n");
	_malloc(BLOCK_SIZE);
	void* content2 = _malloc(BLOCK_SIZE);
	void* content3 = _malloc(BLOCK_SIZE);
	_malloc(BLOCK_SIZE);
	struct block_header* block2 = block_get_header(content2);
	struct block_header* block3 = block_get_header(content3);
	debug_heap(stdout,heap_addr);
	fprintf(stdout, "\n---Free second block---\n");
	_free(content2);
	_free(content3);
	debug_heap(stdout,heap_addr);	
	if (!block2->is_free || !block3->is_free){
		fprintf(stderr, "\tMemory release error\n");
		heap_free(heap_addr);
		return false;
	}
	heap_free(heap_addr);
	return true;
}

bool expand_region(){
	fprintf(stdout, "\n\n\tTest expand_region\n\n---Init heap---\n");
	void* heap_addr = heap_init(HEAP_SIZE);
	debug_heap(stdout,heap_addr);
	fprintf(stdout, "\n---Memory allocation---\n");
	void* content1 = _malloc(HEAP_SIZE);
	struct block_header* block1 = block_get_header(content1);
	debug_heap(stdout,heap_addr);
	fprintf(stdout, "\n---Expand region---\n");
	void* content2 = _malloc(BLOCK_SIZE);
	struct block_header* block2 = block_get_header(content2);
	debug_heap(stdout,heap_addr);
	if (content2==NULL || block1->next!=block2 || block2 !=(void *) block1->contents + block1->capacity.bytes){
		fprintf(stderr, "\tRegion expansion error\n");
		heap_free(heap_addr);
		return false;
	}
	heap_free(heap_addr);
	return true;
}


bool init_second_region(){
	fprintf(stdout, "\n\n\tTest init_second_region\n\n---Init heap---\n");
	void* heap_addr = heap_init(HEAP_SIZE);
	debug_heap(stdout,heap_addr);
	fprintf(stdout, "\n---Memory allocation---\n");
	void* content1 = _malloc(HEAP_SIZE);
	struct block_header* block1 = block_get_header(content1);
	debug_heap(stdout,heap_addr);
	void* after_block = (void*)block1->contents + block1->capacity.bytes;
	void* mapped_region = mmap(after_block, HEAP_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED | MAP_ANONYMOUS,-1,0);
	if (mapped_region == MAP_FAILED){
		fprintf(stderr, "\tRegion mapping error\n");
		heap_free(heap_addr);
		return false;
	}
	fprintf(stdout, "\n---Create second rregion---\n");
	void* content2 = _malloc(BLOCK_SIZE);
	struct block_header* block2 = block_get_header(content2);
	debug_heap(stdout,heap_addr);
	if (content2 == NULL || block2->capacity.bytes != BLOCK_SIZE|| block2 == (void *) block1->contents + block1->capacity.bytes){
		fprintf(stderr, "\tRegion mapping error\n");
		heap_free(heap_addr);
		munmap(mapped_region, HEAP_SIZE);
		return false;
	}
	heap_free(heap_addr);
	munmap(mapped_region, HEAP_SIZE);
	return true;
}

size_t tests_count = 5;
my_test tests[] = {
		malloc_block,
		free_one_block,
		free_some_blocks,
		expand_region,
		init_second_region
};

bool tester(){
	size_t tests_passed=0;
	for (size_t i =0; i<tests_count; i++){
		bool result = tests[i]();
        tests_passed += result;
        printf("\n -----------------    ");
        printf( "Test #%" PRId64 " %s", i + 1, result ? "succeed" : "failed");
        printf("    ----------------- \n");

	}
	printf("\n ---------------------- %"PRId64 "/5 tests passed ----------------------\n", tests_count);
	return tests_passed==5?true:false;
}

